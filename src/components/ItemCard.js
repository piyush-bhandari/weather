import { Card, CardContent, Typography } from "@material-ui/core";

export default function ItemCard(props) { // based on the prop isDetailsCard, conditionally render data to make the component reusable
    const { city, setSelectedCity, isDetailsCard, day } = props;
    return (
        <div className='cityCard'>
            <Card onClick={() => !isDetailsCard ? setSelectedCity(city.name) : {}} >
                <CardContent style={{ backgroundColor: '#80808054' }}>
                    <Typography color='textSecondary'>
                        {isDetailsCard ? `${new Date(day.date).getUTCDate()}-${new Date(day.date).getUTCMonth() + 1}` : city.name}
                    </Typography>
                    <Typography variant='h6'>{isDetailsCard ? `Temp: ${day.temp}` : city.temp}</Typography>
                    {isDetailsCard
                        ? <>
                            <Typography color='textSecondary'>Sea level: {day.seaLevel}</Typography>
                        </>
                        : <>
                            <Typography color='textSecondary'>Sunrise: {new Date(city.sunrise).toLocaleTimeString()}</Typography>
                            <Typography color='textSecondary'>Sunset: {new Date(city.sunset).toLocaleTimeString()}</Typography>
                        </>
                    }
                </CardContent>
            </Card>
        </div>
    )
}