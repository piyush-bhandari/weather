import axios from "axios";
import { useEffect, useState } from "react"
import ItemCard from "./ItemCard";

export default function CityDetails(props) {

    useEffect(() => {
        getLocationData(); // fetch 5 day forecast for the selected location on load
    }, []);

    const [dailyForecast, setDailyForecast] = useState([]);

    const getLocationData = async () => {
        const { data } = await axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${props.selectedCity}&appid=${props.apiKey}`);

        const forecast = [];
        data.list.forEach(item => new Date(item.dt_txt).getUTCHours() === 9 ? forecast.push({ temp: item.main.temp, seaLevel: item.main.sea_level, date: item.dt_txt }) : null); // take only entries for 9 AM from the list
        setDailyForecast(forecast);
    };

    return (
        <>
            <h1>Daily forecast for {props.selectedCity} at 9 AM</h1>
            <div className='cityListContainer'>
                {dailyForecast.map((day, index) => <ItemCard key={index} isDetailsCard day={day} />)}
            </div>
        </>
    )
}