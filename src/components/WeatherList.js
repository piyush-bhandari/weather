import ItemCard from "./ItemCard";

export default function WeatherList(props) { // used a reusable component itemCard to render diff city details.
    return (
        <>
            <h1>Cities</h1>
            <div className='cityListContainer'>
                {props.cityList && props.cityList.map(city => <ItemCard key={`cityItem-${city.name}`} city={city} setSelectedCity={props.setSelectedCity} />)}
            </div>
        </>
    )
}