import { Button } from '@material-ui/core';
import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import CityDetails from './components/CityDetails';
import WeatherList from './components/WeatherList';

function App() {

  const apiKey = 'f36802938cddbd53aadbb86ba1c67c4b';

  useEffect(() => {
    getInitialWeatherData();
  }, []);

  const [cityList, setCityList] = useState([]);
  const [selectedCity, setSelectedCity] = useState('');

  const getInitialWeatherData = async () => {

    const city1 = axios.get(`http://api.openweathermap.org/data/2.5/weather?q=London&appid=${apiKey}`);
    const city2 = axios.get(`http://api.openweathermap.org/data/2.5/weather?q=Paris&appid=${apiKey}`);
    const city3 = axios.get(`http://api.openweathermap.org/data/2.5/weather?q=Amsterdam&appid=${apiKey}`);
    const city4 = axios.get(`http://api.openweathermap.org/data/2.5/weather?q=Tokyo&appid=${apiKey}`);
    const city5 = axios.get(`http://api.openweathermap.org/data/2.5/weather?q=Venice&appid=${apiKey}`);

    const resp = await Promise.all([city1, city2, city3, city4, city5]);
    const finalList = resp.map(city => ({ name: city.data.name, temp: city.data.main.temp, country: city.data.sys.country, sunrise: city.data.sys.sunrise, sunset: city.data.sys.sunset }));

    setCityList(finalList);
  };

  return ( // created 2 components to show list of cities and 5 day weather for the selected city
    <div className="App">
      {selectedCity === '' ? <WeatherList cityList={cityList} setSelectedCity={setSelectedCity} /> : null}
      {selectedCity !== '' ? <CityDetails selectedCity={selectedCity} apiKey={apiKey} /> : null}
      <div className='button'>{selectedCity !== '' ? <Button variant='contained' color='primary' onClick={() => setSelectedCity('')}>Back</Button> : null}</div>
    </div>
  );
}

export default App;
